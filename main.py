import easygui, fractions, PIL, numpy

# Choix de l'image
extensions = ["jpg", "jpeg", "png", "svg", "bmp", "ico"]
file = easygui.fileopenbox()
try:
    while file[max([i for i, c in enumerate(file) if c == "."]) + 1:] not in extensions:
        print("Veuillez choisir un format d'image correct : ")
        [print(f"- {i}") for i in extensions]
        file = easygui.fileopenbox()
except:
    pass

if not file:
    print(f"File : {file}")
else:
    # Redimensionnement
    image = PIL.Image.open(file)
    size_base = image.size
    multiplier = float(input("Choisissez le pourcentage de taille de l'image : ")) / 100
    image = image.resize((int(multiplier * size_base[0]), int(multiplier * size_base[1])))
    
    # Lecture de l'image
    image_array = numpy.array(image)
    #print(image_array)

    # Sauvegarde de l'image + Infos
    folder = easygui.filesavebox("Enregistrer le fichier")
    if folder:
        if image.mode == "RGBA":
            folder += '.png'
        else:
            folder += '.jpg'
        PIL.Image.fromarray(image_array).save(f'{folder}')
        print(f"Input File :        {file}")
        print(f"Output File :       {folder}")
        print(f"Color Mode :        {image.mode}")
        print(f"Original Size :     {size_base} px")
        print(f"Size Multiplier :   {multiplier} x")
        print(f"Size Quantum :      {size_base[0] / fractions.Fraction(size_base[0], size_base[1]).numerator / 100} px")
        print(f"Size Percent :      {multiplier * 100} %")
        print(f"New Size :          {image.size} px")

"""
TO DO :
interface de gestion easygui
    bouton "conserver les proportions" pour les dimensions
    prévisualisation de l'image de base
    bouton "enregistrer"
    bouton "sélectionner"
    champs "dimensions"
convertir les pixels en couleur
ajouter les couleurs des blocs minecraft
convertir en fichier schematic

https://apprendrepython.com/traitement-dimages-avec-python-numpy/
https://easygui.readthedocs.io/en/latest/tutorial.html
"""